const axios = require("axios");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const uri =
  "mongodb+srv://root:fRBuxtJPQAWrvjXL@exercise2.vtmejew.mongodb.net/miniproject?retryWrites=true&w=majority";

const { URL, URLmain } = require("./variables");

// Barangay Model
const BarangaySchema = mongoose.model(
  "Barangay",
  new Schema({
    name: { type: String, required: true },
    parentId: { type: Schema.ObjectId, required: true },
  })
);

// Municipality Model
const MunicipalitySchema = mongoose.model(
  "Municipality",
  new Schema({
    name: { type: String, required: true },
    parentId: { type: Schema.ObjectId, required: true },
  })
);

// Province Model
const ProvinceSchema = mongoose.model(
  "Province",
  new Schema({
    name: { type: String, required: true },
  })
);

module.exports = async () => {
  await mongoose.connect(uri);
  const getParentId = await axios.get(`${URLmain}`);
  const response = getParentId.data.data;
  let provinceInput;

  try {
    provinceInput = response.parentOptions.map((province) => {
      return new ProvinceSchema({ name: province });
    });
    const provinceOutput = await ProvinceSchema.insertMany(provinceInput);
    console.log(`Added ${provinceOutput.length} provinces`);
  } catch (err) {
    console.log(`Error in adding provinces: ${err.message}`);
  }

  let municipalityArray = [];
  try {
    provinceInput.forEach((province) => {
      const provinceName = province.name;
      const provinceId = province._id;
      if (response.childOptions && response.childOptions[provinceName]) {
        municipalityArray.push(
          ...response.childOptions[provinceName].map((municipality) => {
            return new MunicipalitySchema({
              name: municipality,
              parentId: provinceId,
            });
          })
        );
      }
    });
  } catch (err) {
    console.log(`Error in retrieving provinces: ${err.message}`);
  }

  try {
    const municipalityOutput = await MunicipalitySchema.insertMany(
      municipalityArray
    );
    console.log(`Added ${municipalityOutput.length} municipalities`);
  } catch (err) {
    console.log(`Error in adding municipalities: ${err.message}`);
  }

  let barangayArray = [];
  try {
    for (municipalityIndex in municipalityArray) {
      if (
        municipalityArray[municipalityIndex] &&
        municipalityArray[municipalityIndex].parentId &&
        municipalityArray[municipalityIndex].name
      ) {
        const province = provinceInput.find((province) => {
          return (
            province._id.toString() ===
            municipalityArray[municipalityIndex].parentId.toString()
          );
        });
        if (province && province.name) {
          const getBarangays = await axios
            .get(`${URL}`, {
              params: {
                parentOption: province.name,
                childOption: municipalityArray[municipalityIndex].name,
              },
            })
            .catch((error) => {
              console.log(
                "error: ",
                error.response.data,
                province.name,
                municipalityArray[municipalityIndex].name
              );
            });

          if (getBarangays && getBarangays.data && getBarangays.data.data) {
            const barangays = getBarangays.data.data;
            barangays.forEach((barangay) => {
              barangayArray.push(
                new BarangaySchema({
                  name: barangay,
                  parentId: municipalityArray[municipalityIndex]._id,
                })
              );
            });
          }
        }
      }
    }
  } catch (err) {
    console.log(`Error retrieving municipalities: ${err.message}`);
  }

  try {
    const barangayOutput = await BarangaySchema.insertMany(barangayArray);
    console.log(`Added ${barangayOutput.length} barangays`);
  } catch (err) {
    console.log(`Error in adding barangays: ${err.message}`);
  }
};
