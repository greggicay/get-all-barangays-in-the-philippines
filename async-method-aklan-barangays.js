const axios = require("axios");
const csv = require("@fast-csv/format");

const path = require("path");

const variables = require("./variables");
const URL = variables.URL;
const URLmain = variables.URLmain;
const province = variables.province;
const CsvFile = require("./csv-file");
const { create } = require("combined-stream");

const csvFile = new CsvFile({
  path: path.resolve(__dirname, "aklan-barangays-async.csv"),
  headers: ["id", "name", "parentId"],
});

let barangayId = 0;
let createCsvFile = true;
function storeBarangaysToCSV(barangays, parentId) {
  let locationArray = [];
  barangays.forEach((location) => {
    locationArray.push({ id: barangayId, name: location, parentId: parentId });
    barangayId++;
  });

  if (createCsvFile) {
    csvFile.create(locationArray);
    createCsvFile = false;
  } else csvFile.append(locationArray);
}

module.exports = async () => {
  const getParentId = await axios.get(`${URLmain}`);
  const response = getParentId.data.data;
  const provinceId = Object.keys(response.childOptions).findIndex(
    (location) => location === province
  );
  const parentIds = Object.values(response.childOptions[province]).map(
    (municipality, index) => {
      return [
        municipality,
        `${province}-${provinceId}-${municipality}-${index}`,
      ];
    }
  );

  for (const parentIndex in parentIds) {
    const getBarangays = await axios
      .get(`${URL}`, {
        params: {
          parentOption: province,
          childOption: parentIds[parentIndex][0],
        },
      })
      .catch((error) => console.log("error:", error.response.data));
    const barangays = getBarangays.data.data;
    storeBarangaysToCSV(barangays, parentIds[parentIndex][1]);
  }
};
