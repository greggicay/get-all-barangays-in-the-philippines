const axios = require("axios");
const { MongoClient } = require("mongodb");
const uri =
  "mongodb+srv://root:fRBuxtJPQAWrvjXL@exercise2.vtmejew.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(uri);

const variables = require("./variables");
const URL = variables.URL;
const URLmain = variables.URLmain;

const storeBarangays = async (locationArray) => {
  try {
    const database = client.db("miniproject");
    const barangays = database.collection("barangays");
    const options = { ordered: true };
    const result = await barangays.insertMany(locationArray, options);
    console.log(`${result.insertedCount} documents were inserted`);
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
};

module.exports = async () => {
  const getParentId = await axios.get(`${URLmain}`);
  const response = getParentId.data.data;
  const parentIds = [];
  Object.keys(response.childOptions).forEach((province, provinceIndex) => {
    parentIds.push(
      ...Object.values(response.childOptions[province]).map(
        (municipality, index) => {
          return [
            province,
            municipality,
            `${province}-${provinceIndex}-${municipality}-${index}`,
          ];
        }
      )
    );
  });

  let storeBarangay = true;
  const locationArray = [];

  for (const parentIndex in parentIds) {
    const getBarangays = await axios
      .get(`${URL}`, {
        params: {
          parentOption: parentIds[parentIndex][0],
          childOption: parentIds[parentIndex][1],
        },
      })
      .catch((error) => {
        console.log(
          "error:",
          error.response.data,
          parentIds[parentIndex][0],
          parentIds[parentIndex][1]
        );

        storeBarangay = false;
      });

    if (storeBarangay) {
      const barangays = getBarangays.data.data;
      locationArray.push(
        ...barangays.map((barangay) => {
          return { name: barangay, parentId: parentIds[parentIndex][2] };
        })
      );
    }
    storeBarangay = true;
  }
  storeBarangays(locationArray).catch(console.dir);
};
