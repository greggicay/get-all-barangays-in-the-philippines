const variables = require("./variables");
const URL = variables.URL;
const URLmain = variables.URLmain;
const province = variables.province;

const callback = (error, response, body) => {
  if (response.statusCode !== 200) console.log("error:", error);
  else {
    const locations = JSON.parse(body).data;
    const provinceId = `${province}-${Object.keys(
      locations.childOptions
    ).findIndex((location) => location === province)}`;
    const municipalities = Object.values(locations.childOptions[province]);

    console.log("ID", "Name", "Parent ID");
    let barangayId = 0;
    municipalities.forEach((municipality, municipalityIndex) => {
      request(
        {
          uri: `${URL}`,
          qs: {
            parentOption: province,
            childOption: municipality,
          },
        },
        (error, response, body) => {
          if (response.statusCode !== 200) console.log("error:", error);
          else {
            const locations = JSON.parse(body).data;
            const municipalityId = `${municipality}-${municipalityIndex}`;
            if (locations.length !== 0) {
              locations.forEach((location) => {
                console.log(
                  barangayId,
                  location,
                  `${provinceId}-${municipalityId}`
                );
                barangayId++;
              });
            }
          }
        }
      );
    });
  }
};

const request = require("request");
module.exports = () => {
  request(
    {
      method: "GET",
      uri: `${URLmain}`,
    },
    callback
  );
};
