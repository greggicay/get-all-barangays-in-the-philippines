const axios = require("axios");
const csv = require("@fast-csv/format");

const path = require("path");

const variables = require("./variables");
const URL = variables.URL;
const URLmain = variables.URLmain;
const municipality = variables.municipality;
const province = variables.province;
const CsvFile = require("./csv-file");

const parentId = axios
  .get(`${URLmain}`)
  .then((response) => {
    const locations = response && response.data && response.data.data;
    const provinceId =
      locations.childOptions &&
      Object.keys(locations.childOptions) &&
      Object.keys(locations.childOptions).findIndex(
        (location) => location === province
      );
    const municipalityId =
      locations.childOptions &&
      locations.childOptions[province] &&
      locations.childOptions[province].findIndex(
        (location) => location === municipality
      );
    return `${province}-${provinceId}-${municipality}-${municipalityId}`;
  })
  .catch((error) => console.log(error));

module.exports = () => {
  axios
    .get(`${URL}`, {
      params: {
        parentOption: province,
        childOption: municipality,
      },
    })
    .then((response) => {
      const responseData = response && response.data && response.data.data;
      let csvFile;
      try {
        csvFile = new CsvFile({
          path: path.resolve(__dirname, "promise-barangay.csv"),
          headers: ["id", "name", "parentId"],
        });
      } catch (error) {
        console.log(error);
      }

      parentId.then((result) => {
        const locationArray = responseData.map((location, index) => {
          return { id: index, name: location, parentId: result };
        });
        csvFile.create(locationArray);
      });
    })
    .catch((error) => console.log(error));
};
