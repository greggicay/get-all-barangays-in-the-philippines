const province = 'Aklan';
const municipality = 'Kalibo';

const URLmain = 'https://demo.myruntime.com/sustainability-run/fulfillmentClustersService/api/getPhilClusters/sustainabilityRun';
const URL = 'https://demo.myruntime.com/sustainability-run/fulfillmentClustersService/api/getPhilClusterOptions/sustainabilityRun';

module.exports.province = province;
module.exports.municipality = municipality;
module.exports.URL = URL;
module.exports.URLmain = URLmain;