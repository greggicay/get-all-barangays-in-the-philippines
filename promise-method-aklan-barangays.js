const axios = require("axios");
const csv = require("@fast-csv/format");

const path = require("path");

const variables = require("./variables");
const URL = variables.URL;
const URLmain = variables.URLmain;
const province = variables.province;
const CsvFile = require("./csv-file");

const csvFile = new CsvFile({
  path: path.resolve(__dirname, "promise-aklan-barangays.csv"),
  headers: ["id", "name", "parentId"],
});

module.exports = () => {
  axios
    .get(`${URLmain}`)
    .then((response) => {
      const locations = response.data.data;
      const provinceId = Object.keys(locations.childOptions).findIndex(
        (location) => location === province
      );
      const municipalities = Object.values(locations.childOptions[province]);
      const parentIds = [];
      municipalities.forEach((municipality, index) =>
        parentIds.push([
          municipality,
          `${province}-${provinceId}-${municipality}-${index}`,
        ])
      );
      return parentIds;
    })
    .then((parentIds) => {
      const promises = parentIds.map((parentId) => {
        return axios
          .get(`${URL}`, {
            params: {
              parentOption: province,
              childOption: parentId[0],
            },
          })
          .then((response) => {
            return { parentId: parentId[1], barangays: response.data.data };
          });
      });

      Promise.all(promises).then((responses) => {
        let locationArray = [];
        let barangayId = 0;
        responses.forEach((response) => {
          response.barangays.forEach((barangay) => {
            locationArray.push({
              id: barangayId,
              name: barangay,
              parentId: response.parentId,
            });
            barangayId++;
          });
        });
        csvFile.create(locationArray).catch((err) => {
          console.error(err.stack);
          process.exit(1);
        });
      });
    })
    .catch((error) => console.log(error));
};
