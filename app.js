// <===========CALLBACK METHOD===========>
// const getOutputByCallback = require("./callback-method");
// getOutputByCallback();
// <===========CALLBACK METHOD===========>

// <===========CALLBACK METHOD ALL AKLAN BARANGAYS===========>
// const getOutputByCallback = require("./callback-method-aklan-barangays");
// getOutputByCallback();
// <===========CALLBACK METHOD ALL AKLAN BARANGAYS===========>

// <===========PROMISE METHOD===========>
// const getOutputByPromise = require("./promise-method");
// getOutputByPromise();
// <===========PROMISE METHOD===========>

// // <===========PROMISE METHOD ALL AKLAN BARANGAYS===========>
// const getOutputByPromise = require("./promise-method-aklan-barangays");
// getOutputByPromise();
// // <===========PROMISE METHOD ALL AKLAN BARANGAYS===========>

// <===========ASYNC/AWAIT METHOD===========>
// const getOutputByAsync = require("./async-method");
// getOutputByAsync();
// <===========ASYNC/AWAIT METHOD===========>

// <===========ASYNC/AWAIT METHOD ALL AKLAN BARANGAYS===========>
// const getOutputByAsync = require("./async-method-aklan-barangays");
// getOutputByAsync();
// <===========ASYNC/AWAIT METHOD ALL AKLAN BARANGAYS===========>

// <===========ASYNC/AWAIT METHOD ALL PH BARANGAYS===========>
// const getOutputByAsync = require("./async-method-ph-barangays");
// getOutputByAsync();
// <===========ASYNC/AWAIT METHOD ALL PH BARANGAYS===========>

// <===========ASYNC/AWAIT METHOD===========>
// const getOutputByAsync = require("./async-method-with-mongodb");
// getOutputByAsync();
// <===========ASYNC/AWAIT METHOD===========>

// <===========ASYNC/AWAIT METHOD===========>
// const getOutputByAsync = require("./async-method-aklan-barangays-with-mongodb");
// getOutputByAsync();
// <===========ASYNC/AWAIT METHOD===========>

// <===========ASYNC/AWAIT METHOD ALL PH BARANGAYS===========>
// const getOutputByAsync = require("./async-method-ph-barangays-with-mongodb");
// getOutputByAsync();
// <===========ASYNC/AWAIT METHOD ALL PH BARANGAYS===========>

// <===========ASYNC/AWAIT METHOD ALL PH BARANGAYS===========>
// const getOutputByAsync = require("./async-method-save-all-to-mongodb");
// getOutputByAsync();
// <===========ASYNC/AWAIT METHOD ALL PH BARANGAYS===========>

// <===========MONGOOSE METHOD ALL PH BARANGAYS===========>
const getOutputByAsync = require("./mongoose-method");
getOutputByAsync();
// <===========MONGOOSE METHOD ALL PH BARANGAYS===========>
