const axios = require("axios");
const csv = require("@fast-csv/format");

const path = require("path");

const variables = require("./variables");
const URL = variables.URL;
const URLmain = variables.URLmain;
const CsvFile = require("./csv-file");
const { create } = require("combined-stream");
const { error } = require("console");

const csvFile = new CsvFile({
  path: path.resolve(__dirname, "ph-barangays-async.csv"),
  headers: ["id", "name", "parentId"],
});

let barangayId = 0;
let createCsvFile = true;
function storeBarangaysToCSV(barangays, parentId) {
  let locationArray = [];
  barangays.forEach((location) => {
    locationArray.push({ id: barangayId, name: location, parentId: parentId });
    barangayId++;
  });

  if (createCsvFile) {
    csvFile.create(locationArray);
    createCsvFile = false;
  } else csvFile.append(locationArray);
}

module.exports = async () => {
  const getParentId = await axios.get(`${URLmain}`);
  const response = getParentId.data.data;
  const parentIds = [];
  Object.keys(response.childOptions).forEach((province, provinceIndex) => {
    parentIds.push(
      ...Object.values(response.childOptions[province]).map(
        (municipality, index) => {
          return [
            province,
            municipality,
            `${province}-${provinceIndex}-${municipality}-${index}`,
          ];
        }
      )
    );
  });

  let storeBarangay = true;
  for (const parentIndex in parentIds) {
    const getBarangays = await axios
      .get(`${URL}`, {
        params: {
          parentOption: parentIds[parentIndex][0],
          childOption: parentIds[parentIndex][1],
        },
      })
      .catch((error) => {
        console.log(
          "error:",
          error.response.data,
          parentIds[parentIndex][0],
          parentIds[parentIndex][1]
        );

        storeBarangay = false;
      });

    if (storeBarangay) {
      const barangays = getBarangays.data.data;
      storeBarangaysToCSV(barangays, parentIds[parentIndex][2]);
    }
    storeBarangay = true;
  }
};
