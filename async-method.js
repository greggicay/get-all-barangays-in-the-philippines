const axios = require("axios");
const csv = require("@fast-csv/format");

const path = require("path");

const variables = require("./variables");
const URL = variables.URL;
const URLmain = variables.URLmain;
const municipality = variables.municipality;
const province = variables.province;
const CsvFile = require("./csv-file");

function storeBarangaysToCSV(barangays, parentId) {
  const csvFile = new CsvFile({
    path: path.resolve(__dirname, "barangay-async.csv"),
    headers: ["id", "name", "parentId"],
  });

  let locationArray = [];
  barangays.forEach((location, index) =>
    locationArray.push({ id: index, name: location, parentId: parentId })
  );

  csvFile.create(locationArray);
}

module.exports = async () => {
  const getParentId = await axios.get(`${URLmain}`);
  const response = getParentId.data.data;
  const provinceId = Object.keys(response.childOptions).findIndex(
    (location) => location === province
  );
  const municipalityId = Object.values(
    response.childOptions[province]
  ).findIndex((location) => location === municipality);
  const parentId = `${province}-${provinceId}-${municipality}-${municipalityId}`;
  const getBarangays = await axios.get(`${URL}`, {
    params: { parentOption: province, childOption: municipality },
  });
  const barangays = getBarangays.data.data;
  storeBarangaysToCSV(barangays, parentId);
};
