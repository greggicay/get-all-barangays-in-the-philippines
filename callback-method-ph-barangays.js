const variables = require("./variables");
const URL = variables.URL;
const URLmain = variables.URLmain;

const callback = (error, response, body) => {
  if (response.statusCode !== 200) console.log("error:", error);
  else {
    const locations = JSON.parse(body).data;
    const provinces = Object.keys(locations.childOptions);
    console.log("ID", "Name", "Parent ID");
    let barangayId = 0;
    provinces.forEach((province, provinceIndex) => {
      const provinceId = `${province}-${provinceIndex}`;
      const municipalities = Object.values(locations.childOptions[province]);

      municipalities.forEach((municipality, municipalityIndex) => {
        request(
          {
            method: "GET",
            uri: `${URL}`,
            qs: {
              parentOption: province,
              childOption: municipality,
            },
          },
          (error, response, body) => {
            if (response.statusCode !== 200) console.log("error:", error);
            else {
              const locations = JSON.parse(body).data;
              const municipalityId = `${municipality}-${municipalityIndex}`;
              if (locations.length !== 0) {
                locations.forEach((location) => {
                  console.log(
                    barangayId,
                    location,
                    `${provinceId}-${municipalityId}`
                  );
                  barangayId++;
                });
              }
            }
          }
        );
      });
    });
  }
};

const request = require("request");
module.exports = () => {
  request(
    {
      method: "GET",
      uri: `${URLmain}`,
    },
    callback
  );
};
