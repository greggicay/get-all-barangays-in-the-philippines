const axios = require("axios");
const { MongoClient } = require("mongodb");
const uri =
  "mongodb+srv://root:fRBuxtJPQAWrvjXL@exercise2.vtmejew.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(uri);

const variables = require("./variables");
const URL = variables.URL;
const URLmain = variables.URLmain;
const municipality = variables.municipality;
const province = variables.province;

const storeBarangays = async (locationArray) => {
  try {
    const database = client.db("miniproject");
    const barangays = database.collection("barangays");
    const options = { ordered: true };
    const result = await barangays.insertMany(locationArray, options);
    console.log(`${result.insertedCount} documents were inserted`);
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
};

function createLocationArray(barangays, parentId) {
  let locationArray = [];
  barangays.forEach((location) =>
    locationArray.push({ name: location, parentId: parentId })
  );

  return locationArray;
}

module.exports = async () => {
  const getParentId = await axios.get(`${URLmain}`);
  const response = getParentId.data.data;
  const provinceId = Object.keys(response.childOptions).findIndex(
    (location) => location === province
  );
  const municipalityId = Object.values(
    response.childOptions[province]
  ).findIndex((location) => location === municipality);
  const parentId = `${province}-${provinceId}-${municipality}-${municipalityId}`;
  const getBarangays = await axios.get(`${URL}`, {
    params: { parentOption: province, childOption: municipality },
  });
  const barangays = getBarangays.data.data;
  const locationArray = createLocationArray(barangays, parentId);
  storeBarangays(locationArray).catch(console.dir);
};
