const axios = require("axios");
const { MongoClient } = require("mongodb");
const uri =
  "mongodb+srv://root:fRBuxtJPQAWrvjXL@exercise2.vtmejew.mongodb.net/miniproject?retryWrites=true&w=majority";
const client = new MongoClient(uri);

const variables = require("./variables");
const URL = variables.URL;
const URLmain = variables.URLmain;

const storeLocations = async (locationArray, collectionName) => {
  try {
    const database = client.db("miniproject");
    const collection = database.collection(collectionName);
    const options = { ordered: true };
    const result = await collection.insertMany(locationArray, options);
    console.log(
      `${result.insertedCount} documents were inserted to ${collectionName}`
    );
  } catch (error) {
    console.log(error);
  }
};

const getLocations = async (collectionName) => {
  try {
    const database = client.db("miniproject");
    const locations = database.collection(collectionName);
    const cursor = locations.find();

    const result = [];
    for await (const doc of cursor) {
      result.push(doc);
    }
    return result;
  } catch (error) {
    console.log(error);
  }
};

module.exports = async () => {
  const getParentId = await axios.get(`${URLmain}`);
  const response = getParentId.data.data;
  const municipalities = [];

  await storeLocations(
    response.parentOptions.map((province) => {
      return { name: province };
    }),
    "provinces"
  );
  const provinceIds = await getLocations("provinces");
  provinceIds.forEach((item) => {
    const province = item["name"];
    const provinceId = item["_id"];
    municipalities.push(
      ...Object.values(response.childOptions[province]).map((municipality) => {
        return { name: municipality, parentId: provinceId };
      })
    );
  });

  await storeLocations(municipalities, "municipalities");
  const municipalityIds = await getLocations("municipalities");
  let parentIds = municipalityIds.map((item) => {
    const municipalityId = item["_id"];
    const municipalityName = item["name"];
    const province = provinceIds.find(
      (provinceId) =>
        provinceId["_id"].toString() === item["parentId"].toString()
    );
    return {
      province: province.name,
      municipality: municipalityName,
      municipalityId: municipalityId,
    };
  });

  const barangayArray = [];

  for (const parentIndex in parentIds) {
    const getBarangays = await axios
      .get(`${URL}`, {
        params: {
          parentOption: parentIds[parentIndex].province,
          childOption: parentIds[parentIndex].municipality,
        },
      })
      .catch((error) => {
        console.log(
          "error:",
          error.response.data,
          parentIds[parentIndex].province,
          parentIds[parentIndex].municipality
        );
      });
    if (getBarangays && getBarangays.data && getBarangays.data.data) {
      const barangays = getBarangays.data.data;
      barangays.forEach((barangay) => {
        barangayArray.push({
          name: barangay,
          parentId: parentIds[parentIndex].municipalityId,
        });
      });
    }
  }
  await storeLocations(barangayArray, "barangays").catch(console.dir);
  await client.close();
};
