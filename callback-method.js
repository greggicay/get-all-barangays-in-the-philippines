const variables = require("./variables");
const { URL, URLmain, municipality, province } = require("./variables");

const callback = (error, response, body) => {
  if (!body || response.statusCode !== 200) {
    console.log("error:", error);
  } else {
    let locations;
    try {
      locations = JSON.parse(body).data;
    } catch (error) {
      console.log("error:", error);
    }
    const provinceId = Object.keys(locations.childOptions).findIndex(
      (location) => location === province
    );
    const municipalityId =
      locations.childOptions &&
      locations.childOptions[province] &&
      locations.childOptions[province].findIndex(
        (location) => location === municipality
      );
    request(
      {
        uri: `${URL}`,
        qs: {
          parentOption: province,
          childOption: municipality,
        },
      },
      (error, response, body) => {
        if (response.statusCode !== 200) {
          console.log("error:", error);
        } else {
          let locations;
          try {
            locations = JSON.parse(body).data;
          } catch (error) {
            console.log(error);
          }
          if (locations && locations.length) {
            console.log("ID", "Name", "Parent ID");
            locations.forEach(
              (location, index) =>
                console.log(
                  index,
                  location,
                  `${province}-${provinceId}-${municipality}-${municipalityId}`
                ),
              0
            );
          }
        }
      }
    );
  }
};

const request = require("request");

module.exports = () => {
  request(
    {
      uri: `${URLmain}`,
    },
    callback
  );
};
